﻿<? @session_start(); ?>
<html dir="rtl">
<?php
require_once('inc.php');
if (!isset($_SESSION['v_user']))
  redirect_rel('index.php', '', 0);
else
{
html_top('نويسندگان', 'حذف اطلاعات نويسنده');

$d_author_id = $_REQUEST['id'];

$del_author_submit = $_POST['del_author_submit'];
if ($del_author_submit)
  del_author($d_author_id);
else
  del_author_form($d_author_id);

html_bottom();
}
?>