<?php
//************************** HTML TOP
function html_top($t = '', $h = '', $p = '')//$t = title; $h = heading; $p = type of menu;
{
	if (!$t)
		$t = 'انّ علي و شيعته هم الفائزون';
	mydbconnect();
	?>
	<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8">
		<meta http-equiv="content-language" content="fa">
		<link href="main.css" rel="stylesheet" type="text/css">
		<link rel="shortcut icon" type="image/x-icon" href="./images/logo.ico" />
		<script type="text/javascript">
			_editor_url = './htmlarea3';
			_editor_lang = 'fa';
		</script>
		<script type="text/javascript" src="./j.js"></script>
		<script type="text/javascript" src="./htmlarea3/htmlarea.js"></script>
		<script type="text/javascript" src="./htmlarea3/dialog.js"></script>
		<script tyle="text/javascript" src="./htmlarea3/lang/fa.js"></script>
		<title><?php echo $t; ?></title>
	</head>
	<body onLoad="HTMLArea.replaceAll();">
	<a name="ptop"></a>
	<table><!-- THIS IS MAIN TABLE OF PAGE -->
		<tr><!-- MAIN TABLE -->
			<td><!-- MAIN TABLE -->
				<?php
				banner($h);
				?>
			</td><!-- MAIN TABLE -->
		</tr><!-- MAIN TABLE -->
		<tr><!-- MAIN TABLE -->
			<td><!-- MAIN TABLE -->
				<?php
				bodytop($h, $p);
}
//************************** HTML TOP - end 

//************************** BANNER
function banner($h = '')
{
	?>
	<table border="0" width="100%" cellpadding="0"
		style="border-collapse: collapse;
		background: url(<?php echo $main_url; ?>images/Main_02.jpg);"><!-- BANNER -->
		<tr>
			<td width="278">
				<a href="<?php echo $main_url; ?>" accesskey="1">
				<img border="0" src="images/Main_01.jpg"></a>
			</td>
			<td>&nbsp;</td>
			<td width="225" align="left">
				<a href="<?php echo $main_url; ?>" accesskey="1">
				<img src="<?php echo $main_url; ?>images/Main_03.jpg"
					alt="www.Bandeh.com Logo" align="left"></a>
			</td>
		</tr>
	</table><!-- BANNER - END -->
	<TABLE bgcolor="#eef3f4" style="border-collapse: collapse;" cellSpacing=1
		cellPadding=0 width="100%" border=0><!-- UNDER BANNER -->
		<TR>
			<TD width="100%">
				<HR style="color:#cce6ff">
				<HR style="color:#cce6ff">
			</TD>
			<TD>
				<TABLE cellPadding=0 align=left border=0><!-- UNDER BANNER 4 LINKS -->
					<TR>
						<TD><IMG src="<?php echo $main_url; ?>images/small.jpg" border=0></TD>
						<TD><A href="<?php echo $main_url; ?>">صفحه&nbsp;اصلي</A></TD>
						<TD>&nbsp;</TD>
						<TD><IMG src="images/small.jpg" border=0></TD>
						<TD><A href="<?php echo $main_url; ?>quran">قــرآن&nbsp;كريم</A></TD>
						<TD>&nbsp;</TD>
						<TD><IMG src="<?php echo $main_url; ?>images/small.jpg" border=0></TD>
						<TD><A href="mailto:rahmani@bandeh.com">تماس&nbsp;با&nbsp;ما</A></TD>
						<TD>&nbsp;</TD>
						<TD><IMG src="<?php echo $main_url; ?>images/small.jpg" border=0></TD>
						<TD><A href="#1">معرفي</A></TD>
					</TR>
				</TABLE><!-- UNDER BANNER 4 LINKS - END -->
			</TD>
		</TR>
	</TABLE><!-- UNDER BANNER - END -->
	<div align="center">
		تشيع | تاريخ اهل بيت ع | باورهاي رستگاران | كتاب‌شناسي | مقاله | مناسبتها | پرسش و پاسخ | فرق و مذاهب اسلامي | اديان | دانشنامه | خبر | شبستان
	</div>
	<?php
}
//************************** BANNER - end

//************************** BODY TOP
function bodytop($h = '', $p = '')
{
	?>
	<table id="page_body"><!-- PAGE BODY -->
		<tr><!-- PAGE BODY -->
			<td width="15%"><!-- PAGE BODY -->
				<?php bodyright($p); ?>
			</td><!-- PAGE BODY -->
			<td width="70%"><!-- PAGE BODY -->
				<? if ($h)
				{ ?>
				<h3 align="center" style="margin-top:10px;">
					<span class="page_head3">&nbsp;&nbsp;<? echo $h; ?>&nbsp;&nbsp;</span>
				</h3>
				<? } ?>
	<?php
}
//************************** BODY TOP - end

//************************** BODY RIGHT
function bodyright($p = '')
{
	show_user_menu($p);
	echo "<hr>\n";
	if (is_set_user())
		show_managementmenu();
}
//************************** BODY RIGHT - end

//************************** SHOW USER MENU
function show_user_menu($p = '')
{
	?>
	<table class="menu" cellspacing="2">
		<tr>
			<th>&nbsp;</th>
			<th>موضوعات</th>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
				<a href="<?php echo $main_url; ?>index.php">
				<nobr>صفحه اصلي</nobr></a>
			</td>
		</tr>
		<?php
		$mcat = 'select id, title from categories';// a where a.pid=0';
		$mcat = mysql_query($mcat);
		$nmcat = mysql_num_rows($mcat);
		for ($i = 0; $i < $nmcat; $i++)
		{
			$mcat1 = mysql_fetch_object($mcat); 
			?>
			<tr>
				<td>&nbsp;</td>
				<td>
					<a href="<? echo $main_url; ?>?cc=<? echo $mcat1->id; ?>">
					<nobr><? echo untitled($mcat1->title); ?></nobr></a>
				</td>
			</tr>
		<? } ?>
		<tr>
			<td>&nbsp;</td>
			<td><a href="<? echo $main_url; ?>archive.php">
			<nobr>آرشيو</nobr></a></td>
		</tr>
	</table>
	<?php
}
//************************** SHOW USER MENU - end

//************************** UNTITLED
function untitled($s)
{
	if ($s)
		return $s;
	else
		return 'بدون عنوان';
}
//************************** UNTITLED - end

//************************** SHOW MANAGEMENT MENU
function show_managementmenu()
{
	?>
	<table class="menu" cellspacing="2">
		<tr>
			<th>&nbsp;</th>
			<th>منوي مديريت</th>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><a href="<? echo $main_url; ?>index.php"><nobr>صفحه اصلي</nobr></a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><a href="<?php echo $main_url; ?>categories.php">دسته بندي</a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><a href="<?php echo $main_url; ?>entries.php">مطالب</a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><a href="<?php echo $main_url; ?>hadithha.php">حديث هفته</a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><a href="<?php echo $main_url; ?>authors.php">نويسندگان</a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
				<a href="" onClick="upload_file_win=window.open('upload.php?tdir=<? 
					echo FILES_DIR; ?>&tmp=1', 'upload_file_win', 'toolbar=no');">
				بارگذاري&nbsp;فايل</a>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><a href="index.php?lo=logout">خروج</a></td>
		</tr>
	</table>
	<?
}
//************************** SHOW MANAGEMENT MENU - end

//************************** BODY LEFT
function bodyleft($user_msg = '')
{
	?>
	<table class="menu" style="font-size: 9px;" cellspacing="2">
		<tr>
			<th>&nbsp;</th><th>جستجو</th>
		</tr>
		<tr>
			<td colspan="2">
				<form name="search_form" method="post" action="search.php">
					<input type="text" name="search_text" size="9">
					<input type="submit" value="جستجو">
				</form>
			</td>
		</tr>
	</table>
	<hr>
	<!--  Last notes -->
	<?
	hadith_hafteh();
	echo "<hr>\n";
	show_last_entries();
	echo "<hr>\n";
	show_user_login($user_msg);
}
//************************** BODY LEFT - end

//************************** HADITH HAFTEH
function hadith_hafteh()
{
	?>
	<table id="hadith" width="95%" class="menu" cellspacing="2">
		<tr>
			<th>&nbsp;</th>
			<th>
				حديث هفته
			</th>
		</tr>
		<?php
		$h = 'select * from hadithha order by id desc';
		$h = mysql_query($h);
		$h = mysql_fetch_object($h);
		?>
		<tr>
			<td colspan="2">
				<span style="color:#00CC00"><strong>
				<?php echo $h->title; ?>
				</strong></span>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<span style="color:#006600">
				<?php echo $h->speaker; ?>:</span>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<span style="color:#006600">
				<?php echo $h->arabic; ?></span>
			</td>
		</tr>
		<tr><td colspan="2"></td></tr>
		<tr>
			<td colspan="2"><?php echo $h->farsi; ?></td>
		</tr>
		<tr>
			<td align="left" colspan="2"><?php echo $h->refrence; ?></td>
		</tr>
	</table>
	<?php
}
//************************** HADITH HAFTEH - end

//************************** SHOW LAST ENTRIES
function show_last_entries()
{
	?>
	<table class="menu" style="font-size: 9px;" cellspacing="2">
		<tr>
			<th>&nbsp;</th><th>آخرين مطالب</th>
		</tr>
		<?php
		$stable = 'entries';
		$lastnotes = 'select id, title from '.$stable.' a'.
			' where a.publish=1 order by cdate desc, id desc limit 10';
		$lastnotes = mysql_query($lastnotes);
		$nlastnotes = mysql_num_rows($lastnotes);
		for ($nl = 0; $nl < $nlastnotes; $nl++)
		{
			$alast = mysql_fetch_object($lastnotes);
			echo "<tr><td colspan=\"2\">\n";
			echo '<b>&bull;</b> <a href="'.$main_url.'index.php?id='.$alast->id.'">';
			echo untitled($alast->title).'</a>';
			echo "</td></tr>\n";
		}
		?>
	</table>
	<?
}
//************************** SHOW LAST ENTRIES - end

//************************** SHOW USER LOGIN
function show_user_login($user_msg = '')
{
	?>
	<table class="menu" cellspacing="2">
		<tr>
			<th width="3">&nbsp;</th><th>ورود</th>
		</tr>
		<tr>
			<td colspan="2">
				<?
				if (!is_set_user())
				{
					?>
					<form name="login_form" method="post" action="index.php?lo=login">
						<label>نام كاربر</label>
						<input type="text" name="user_name"><br>
						<label>رمز عبور</label>
						<input type="password" name="user_pass">
						<div style="text-align:center"><input type="submit" value=" ورود "></div>
					</form>
					<?
				}
				else
				if ($user_msg == '')
					$user_msg = get_user_nname($_SESSION['v_user']).'، خوش آمديد.';
				echo '<div style="text-align:center">'.$user_msg.'</div>';
				?>
			</td>
		</tr>
	</table>
	<?
}
//************************** SHOW USER LOGIN - end

//************************** BODY BOTTOM
function bodybottom($user_msg = '')
{
	?>
			</td> <!-- 70% --><!-- PAGE BODY -->
			<td width="15%"><!-- PAGE BODY -->
				<?php bodyleft($user_msg); ?>
			</td><!-- PAGE BODY -->
		</tr><!-- PAGE BODY -->
	</table><!-- PAGE BODY - END -->
	<?php
}
//************************** BODY BOTTOM - end

//************************** HTML BOTTOM
function html_bottom($user_msg = '')
{
	bodybottom($user_msg);
	?>
			</td><!-- MAIN TABLE -->
		</tr><!-- MAIN TABLE -->
	</table><!-- MAIN TABLE -->
	<a href="#ptop">^بالا^</a>
	</body>
	</html>
	<?php
}
//************************** HTML BOTTOM - end

//************************** HTMLAREA1
function htmlarea1($id = 'TA')
{
	?>
	<script type="text/javascript" defer="1">
	HTMLArea.replace("<?php echo $id; ?>");
	</script>
	<?php
}
//************************** HTMLAREA1 - end

//************************** SET ETC
function set_etc($t, $n)
{
  if (strlen($t) > $n)
    return '...';
  else
    return '';
}
//************************** SET ETC - end

//************************** NEW CATEGORY
function new_category_form()
{
	global $PHP_SELF;
	?>
	<form method="post" action="<? echo $PHP_SELF; ?>">
		<table class="tform" align="center">
			<tr>
				<td>
					<label>عنوان:</label>
					<input type="text" name="category_title" maxlength="20">
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<label>توضیح:</label><br>
					<textarea name="category_description"></textarea>
				</td>
			</tr>
			<tr>
				<td colspan="3" align="center">
					<input type="submit" name="newcategory_submit" value="  ثبت  ">
				</td>
			</tr>
		</table>
	</form>
	<?
}
//************************** NEW CATEGORY - end

//************************** EDIT CATEGORY FORM
function edit_category_form($e_category_id)
{
	global $PHP_SELF;
	$table = 'categories';
	$query = 'select * from '.$table.' a where '.
		'a.id='.$e_category_id;
	$e_category =  mysql_query($query);
	$num = mysql_num_rows($e_category);
	if ($num == 0)
		echo 'رکوردی با این شماره پیدا نشد.';
	else
	{
		$e_category1 = mysql_fetch_object($e_category);
		?>
		<form method="post" action="<? echo $PHP_SELF; ?>?id=<?php echo $e_category_id; ?>">
			<table class="tform" align="center">
				<tr>
					<td>
						<label>عنوان:</label>
						<input type="text" name="category_title" maxlength="50"
								value="<? echo $e_category1->title; ?>">
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<label>توضیح:</label><br>
<textarea name="category_description"><? echo $e_category1->description; ?></textarea>
					</td>
				</tr>
				<tr>
					<td colspan="3" align="center">
						<input type="submit" name="editcategory_submit" value="  ثبت تغییرات  ">
					</td>
				</tr>
			</table>
		</form>
		<?php
	}
}
//************************** EDIT CATEGORY FORM - end

//************************** NEW ENTRY FORM
function new_entry_form()
{
	global $PHP_SELF;
	?>
	<form method="post" action="<? echo $PHP_SELF; ?>">
		<table class="tform" align="center">
			<tr>
				<td>
					<label>عنوان:</label>
					<input type="text" name="entry_title" maxlength="100">
				</td>
				<td>
					<label>دسته:</label>
					<select name="entry_ccode">
						<option value="0">&nbsp;</option>
						<?php
						$ccodes = 'select id, title from categories';
						$ccodes = mysql_query($ccodes);
						$ccno = mysql_num_rows($ccodes);
						for ($i = 1; $i <= $ccno; $i++)
						{
							$cc1 = mysql_fetch_object($ccodes);
							echo '<option value="'.$cc1->id.'">'.$cc1->title.'</option>'."\n";
						}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<label>خلاصه:</label><br>
					<textarea name="entry_summary" class="summary"></textarea>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<label>متن:</label><br>
					<textarea name="entry_ftext"></textarea>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<label>تاريخ:</label>
					<span dir="ltr">
						<input type="text" name="entry_cdate" value="<? echo mydate(); ?>">
					</span>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<label>انتشار بیابد؟</label>
					<input type="checkbox" name="entry_publish">
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<input type="submit" name="newentry_submit" value="   ثبت  ">
				</td>
			</tr>
		</table>
	</form>
	<?
}
//************************** NEW ENTRY FORM - end

//************************** EDIT ENTRY FORM
function edit_entry_form($e_entry_id)
{
	global $PHP_SELF;
	$table = 'entries';
	$query = 'select * from '.$table.' a where '.
		'a.id='.$e_entry_id;
	$e_entry =  mysql_query($query);
	$num = mysql_num_rows($e_entry);
	if ($num == 0)
		echo 'رکوردی با این شماره پیدا نشد.';
	else
	{
		$e_entry1 = mysql_fetch_object($e_entry);
		?>
		<form method="post" action="<? echo $PHP_SELF; ?>?id=<?php echo $e_entry_id; ?>">
			<table class="tform" align="center">
				<tr>
					<td>
						<label>عنوان:</label>
						<input type="text" name="entry_title" maxlength="200"
							value="<? echo $e_entry1->title; ?>">
					</td>
					<td>
						<label>دسته:</label>
						<select name="entry_ccode">
							<option value="0">&nbsp;</option>
							<?php
							$ccodes = 'select id, title from categories';
							$ccodes = mysql_query($ccodes);
							$ccno = mysql_num_rows($ccodes);
							for ($i = 1; $i <= $ccno; $i++)
							{
								$cc1 = mysql_fetch_object($ccodes);
								if ($e_entry1->ccode == $cc1->id)
									$selected = ' selected';
								else
									$selected = '';
								echo '<option value="'.$cc1->id.'"'.$selected.'>'.
									$cc1->title.'</option>'."\n";
							}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<label>خلاصه:</label><br>
<textarea name="entry_summary" class="summary"><? echo $e_entry1->summary; ?></textarea>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<label>متن:</label><br>
						<textarea name="entry_ftext"><? echo $e_entry1->ftext; ?></textarea>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<label>تاريخ:</label>
						<span dir="ltr">
							<input type="text" name="entry_cdate"
								value="<? echo $e_entry1->cdate; ?>">
						</span>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<label>انتشار بیابد؟</label>
						<?php
						if ($e_entry1->publish == 1)
							echo '<input type="checkbox" name="entry_publish" checked>';
						else
							echo '<input type="checkbox" name="entry_publish">';
						?>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center">
						<input type="submit" name="editentry_submit" value="  ثبت تغییرات  ">
					</td>
				</tr>
			</table>
		</form>
		<?php
	}
}
//************************** EDIT ENTRY FORM - end

//************************** WI HI
function wi_hi($fullshow)
{
  if ($fullshow)
    return '';
  else
    return ' width="100" ';
}
//************************** WI HI - end

//************************** SHOW ENTRY
function show_entry($id, $fullshow, $cc = 0)
{
	//global NSUMCHAR_BIG;
	$table = 'entries';
	$r = get_record($table, $id);
	if ($cc)
	{
		$ccfilter = ' and a.ccode='.$cc;
		$ccf = 'cc='.$cc.'&';
	}
	else
	{
		$ccfilter = '';
		$ccf = '';
	}
	?>
	<table>
		<tr>
			<td>
				<b><span style="background:#999999; color:#FFCCCC;">
				<a href="index.php?cc=<? echo $r->ccode; ?>">
				<? echo get_category($r->ccode); ?></a>
				</span></b>&nbsp;
				<a href="<? echo $main_url; ?>index.php?<?
				echo $ccf; ?>&id=<? 
				echo $r->id; ?>">
				<b><? echo $r->title; ?></b></a>
			</td>
		</tr>
		<tr>
			<td>
				<?
				if ($fullshow)
				{
					echo $r->ftext;
					?>
					<table>
						<tr>
							<td align="center">
								<?
								$qn = 'select id from '.$table.' a where a.publish=1 '.$ccfilter.
									' and a.id<'.$r->id.' order by id desc limit 1';
								//echo '<span dir=ltr><font color=red size=4>'.$qn.'</font></span>';
								$qn = mysql_query($qn);
								$qn = mysql_fetch_object($qn);
								if ($qn)
								{
									echo '<a href="index.php?'.$ccf.
										'arch=0&id='.$qn->id.'"><قبلي</a>';
								}
								echo '&nbsp;&nbsp;|&nbsp;&nbsp;';
								$qn = 'select id from '.$table.' a where a.publish=1 '.
									$ccfilter.' and a.id>'.$r->id.' limit 1';
								$qn = mysql_query($qn);
								$qn = mysql_fetch_object($qn);
								if ($qn)
								{
									echo '<a href="index.php?'.$ccf.
										'arch=0&id='.$qn->id.'">بعدي></a>';
								}
								?>
							</td>
						</tr>
					</table>
					<?
				}
				else
				{
					echo $r->summary;
					echo '<br><a href="'.$main_url.'index.php?'.$ccf.
						'&id='.$r->id.'">ادامه مطلب...</a>';
				}
				?>
			</td>
		</tr>
	</table>
	<?
}
//************************** SHOW ENTRY - end

//************************** UPLOAD FORM
function upload_form($target_dir, $tmp = 0)
{
  global $PHP_SELF;
 ?>
  <form method="post" enctype="multipart/form-data"
    action="<? echo $PHP_SELF ?>?tdir=<? echo $target_dir; ?>&tmp=<? echo $tmp; ?>">
    بارگذاري فايل:
    <input type="file" name="userfile">
    <input type="submit" name="upload_submit" value="بارگذاري">
  </form>
  <?php
}
//************************** UPLOAD FORM - end

//************************** NEW HADITH FORM
function new_hadith_form()
{
	global $PHP_SELF;
	?>
	<form method="post" action="<? echo $PHP_SELF; ?>">
		<table class="tform" align="center">
			<tr>
				<td>
					<label>عنوان:</label>
					<input type="text" name="hadith_title" maxlength="100">
				</td>
				<td>
					<label>گوينده:</label>
					<input type="text" name="hadith_speaker" maxlength="100">
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<label>عربي:</label><br>
					<textarea name="hadith_arabic" style="height: 200px;"></textarea>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<label>ترجمه:</label><br>
					<textarea name="hadith_farsi" style="height: 200px;"></textarea>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<label>منبع:</label>
					<input type="text" name="hadith_refrence" maxlength="100">
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<input type="submit" value="   ثبت   " name="newhadith_submit">
				</td>
			</tr>
		</table>
	</form>
	<?
}
//************************** NEW HADITH FORM - end

//************************** NEW AUTHOR FORM
function new_author_form()
{
  global $PHP_SELF;
  if (is_this_manager())
  {
    ?>
    <form name="new_author" method="post" action="<? echo $PHP_SELF; ?>">
      <table class="tform" align="center">
        <tr>
          <td><label>نام:</label></td>
          <td><input type="text" name="author_name" maxlength="100"></td>
        </tr>
        <tr>
          <td><label>نام كاربري:</label></td>
          <td><input type="text" name="author_uname" maxlength="100"></td>
        </tr>
        <tr>
          <td><label>رمز عبور:</label></td>
          <td><input type="password" name="author_pass" maxlength="100"></td>
        </tr>
        <tr>
          <td><label>تأييد رمز عبور:</label></td>
          <td><input type="password" name="author_passc" maxlength="100"></td>
        </tr>
        <tr>
          <td colspan="2" align="center">
            <input type="submit" name="newauthor_submit" value="    ثبت   ">
          </td>
        </tr>
      </table>
    </form>
  <?
  }
  else
  {
    $mymsg = '<br><br>شما اجازه اين كار را نداريد.';
    redirect_rel('authors.php', $mymsg, 0);
  }
}
//************************** NEW AUTHOR FORM - end

//************************** EDIT AUTHOR FORM
function edit_author_form($e_author_id)
{
  global $PHP_SELF;
  $table = 'authors';
  $query = 'select * from '.$table.' a where '.
    'a.id='.$e_author_id;
  $e_authors =  mysql_query($query);
  $num = mysql_num_rows($e_authors);
  if ($num == 0)
    echo 'رکوردی با این شماره پیدا نشد.';
  else
  {
    $e_author1 = mysql_fetch_object($e_authors);
	if ($_SESSION['v_user'] == $e_author1->uname)
	{
      ?>
      <form method="post" action="<? echo $PHP_SELF; ?>?id=<?php echo $e_author_id; ?>">
        <table class="tform" align="center">
          <tr>
            <td><label>نام:</label></td>
            <td><input type="text" name="author_name" maxlength="100"
                value="<? echo $e_author1->name; ?>"></td>
          </tr>
          <tr>
            <td><label>نام كاربري:</label></td>
            <td><input type="text" name="author_uname" maxlength="100"
                value="<? echo $e_author1->uname; ?>"></td>
          </tr>
          <tr>
            <td><label>رمز عبور فعلي:</label></td>
            <td><input type="password" name="author_pass_now" maxlength="100"></td>
          </tr>
          <tr>
            <td><label>رمز عبور جديد:</label></td>
            <td><input type="password" name="author_pass_new" maxlength="100"></td>
          </tr>
          <tr>
            <td><label>تأييد رمز عبور جديد:</label></td>
            <td><input type="password" name="author_pass_new_c" maxlength="100"></td>
          </tr>
          <tr>
            <td colspan="2" align="center">
              <input type="submit" name="editauthor_submit" value="  ثبت تغییرات  ">
            </td>
          </tr>
        </table>
      </form>
      <?php
    }
    else
    {
      $mymsg = '<br><br>شما اجازه اين كار را نداريد.';
      redirect_rel('authors.php', $mymsg, 0);
    }
  }
}
//************************** EDIT AUTHOR FORM - end

//************************** DELETE AUTHOR FORM
function del_author_form($d_author_id)
{
  global $PHP_SELF;
  $q = 'select name, uname from authors a where a.id='.$d_author_id;
  $q = mysql_query($q);
  if (@mysql_num_rows($q))
  {
    if (is_this_manager())
    {
      $q = mysql_fetch_object($q);
      if ($_SESSION['v_user'] == $q->uname)
	  {
        $mymsg = '<br><br><b>شما اجازه اين كار را نداريد.</b>';
        redirect_rel('authors.php', $mymsg, 0);
	  }
      else
      {
        echo '<table class="tform" align="center" cellspacing="3">';
        echo '<tr><td>'.$q->name.'<br>'.$q->uname.'<br><br>';
        echo '<form action="'.$PHP_SELF.'?id='.$d_author_id.'" method="post">';
        echo 'رمز عبور مديريت:<br>'.
          '<input type="password" name="m_pass"><br>'.
          '<input type="submit" name="del_author_submit" value=" حذف ">';
        echo '</form></td></tr></table>';
      }
    }
    else
    {
      $mymsg = '<br><br><b>شما اجازه اين كار را نداريد.</b>';
      redirect_rel('authors.php', $mymsg, 0);
    }
  }
  else
  {
    $mymsg = '<br><br>چنين ركوردي پيدا نشد.<br>';
    redirect_rel('authors.php', $mymsg, 0);
  }
}
//************************** DELETE AUTHOR FORM - end

?>