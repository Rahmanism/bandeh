﻿<? @session_start();
require_once('inc.php');
if (!isset($_SESSION['v_user']))
  redirect_rel('index.php', '', 0);
else
{
html_top('', 'ويرايش مطلب');

$e_entry_id = $_REQUEST['id'];
$editentry_submit = $_POST['editentry_submit'];
if ($editentry_submit)
  update_entry($e_entry_id);
else
  edit_entry_form($e_entry_id);

html_bottom();
}
?>