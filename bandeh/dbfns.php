﻿<?php
//************************** MYDBCONNECT
function mydbconnect()
{
	global $hostname_bandeh;
	global $database_bandeh;
	global $username_bandeh;
	global $password_bandeh;
	$bandehconn = @mysql_pconnect($hostname_bandeh, $username_bandeh, $password_bandeh);
	if (!$bandehconn)
		die('خطا در اتصال به سرور بانک اطلاعاتی');
	if (!@mysql_select_db($database_bandeh))
		die('خطا در برقراری ارتباط با بانک اطلاعاتی');
}
//************************** MYDBCONNECT - end

//************************** IS SET USER
function is_set_user()
{
	if (isset($_SESSION['v_user']))
		return true;
	else
		return false;
}
//************************** IS SET USER - end

//************************** GET CATEGORIES
function get_categories($ml = 'm')//$pid = 0)
{
	$ccodes = 'select id, title from '.$ml.'categories';// a where a.pid='.$pid;
	$ccodes = mysql_query($ccodes);
	$ccno = mysql_num_rows($ccodes);
	return array($ccno, $ccodes);
}
//************************** GET CATEGORIES - end

//************************** GET CATEGORY
function get_category($id = 0)
{
	$a_category = 'select id, title from categories a where a.id='.$id;
	$a_category = mysql_query($a_category);
	if (!@mysql_num_rows($a_category))
		return 0;
	else
	{
		$a_category = mysql_fetch_object($a_category);
		return $a_category->title;
	}
}
//************************** GET CATEGORY - end

//************************** GET CHILD CATEGORIES
function get_child_categories($pid = 0, $e_pid = 0, $new = 1)
{
  $child_categories = 'select id, title from categories a where a.pid='.$pid;
  $child_categories = mysql_query($child_categories);
  $children_no = mysql_num_rows($child_categories);
  if ($children_no > 0)
  {
    global $spaces;
    $spaces .= '&nbsp;';
    for ($i = 0; $i < $children_no; $i++)
    {
      $child_category = mysql_fetch_object($child_categories);
      if (!$new)
      {
            if ($cc1->id == $e_pid)
            $selected = ' selected';
          else
            $selected = '';
      }
        echo '<option value="'.$child_category->id.'"'.$selected.'>'.
        $spaces.$child_category->name.'</option>'."\n";
        get_child_categories($child_category->id, $e_pid, 0);
    }
    $spaces = substr($spaces, 0, strlen($spaces) - 6);
  }
  return;
}
//************************** GET CHILD CATEGORIES - end

//************************** GET LAST ID
function get_last_id($table, $publish = 1, $cc = 'a')
{
	if ($cc == 'a')
		$ccfilter = '';
	else
		$ccfilter = ' and a.ccode='.$cc;
	$q = 'select id from '.$table.' a where a.publish='.$publish.$ccfilter.' order by id desc';
	$q = mysql_query($q);
	$q = mysql_fetch_object($q);
	return $q->id;
}
//************************** GET LAST ID - end

//************************** DELETE VARIABLE
function delete_var($table, $id)
{
	$query= 'select title from '.$table.' a where '.
		'a.id='.$id;
	$d_var =  mysql_query($query);
	$num = mysql_num_rows($d_var);
	if ($num == 0)
		$m = 0;
	else
	{
		$d_var1 = mysql_fetch_object($d_var);
		$query = 'delete from '.$table.' where '.$table.'.id='.$id;
		$d_var = mysql_query($query);
		$num = mysql_affected_rows();
		$m = $num;
	}
	return $m;
}
//************************** DELETE VARIABLE - end

//************************** MYDATE
function mydate()
{
	return date('Y-m-d');
}
//************************** MYDATE - end

//************************** SERVER URL
function server_url()
{   
	$proto = "http" .
		((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "s" : "") . "://";
	$server = isset($_SERVER['HTTP_HOST']) ?
		$_SERVER['HTTP_HOST'] : $_SERVER['SERVER_NAME'];
	return $proto . $server;
}
//************************** SERVER URL - end

//************************** REDIRECT RELATIVE
function redirect_rel($relative_url, $mymsg = '', $haveparam = 0)
{
	$url = server_url() . dirname($_SERVER['PHP_SELF']) . "/" . $relative_url;
	if ($haveparam == 1)
		$m = '&';
	else
		if ($haveparam == 0)
			$m = '?';
	$url .= $m.'mymsg='.$mymsg;
	if (!headers_sent())
		header('Location: $url');
	else
		echo "<meta http-equiv=\"refresh\" content=\"0;url=$url\">\r\n";
}
//************************** REDIRECT RELATIVE - end

//************************** INSERT NEW CATEGORY
function insert_new_category()
{
	$table = 'categories';
	$i_category_title = $_POST['category_title'];
	$i_category_description = $_POST['category_description'];
	$query='insert into '.$table.' (title, description) values ('.
		'"'.$i_category_title.'", "'.$i_category_description.'");';
	$insertnewcategory = mysql_query($query);
	$categoryinsertno = mysql_affected_rows();
	$mymsg = '<br><br><b>'.$categoryinsertno.'</b> رکورد ثبت شد.';

	redirect_rel('categories.php', $mymsg, 0);
}
//************************** INSERT NEW CATEGORY - end

//************************** UPDATE CATEGORY
function update_category($e_category_id)
{
	$table = 'categories';
	$query = 'select title from '.$table.' a where '.
		'a.id='.$e_category_id;
	$e_category = mysql_query($query);
	$num = mysql_num_rows($e_category);
	if ($num == 0)
		echo 'رکوردی با این شماره پیدا نشد.';
	else
	{
		$e_category_title = $_POST['category_title'];
		$e_category_description = $_POST['category_description'];
		$query = 'update '.$table.' set '.
			'title="'.$e_category_title.'", description="'.$e_category_description.
			'" where '.$table.'.id='.$e_category_id;
		$categoryedit = mysql_query($query);
		$categoryeditedno = mysql_affected_rows();
		$mymsg = '<br><br><b>تغییرات '.$categoryeditedno.'</b> رکورد ثبت شد.';
	}

	redirect_rel('categories.php', $mymsg, 0);
}
//************************** UPDATE CATEGORY - end

//************************** INSERT NEW ENTRY
function insert_new_entry()
{
  $table = 'entries';
  $i_entry_ccode = $_POST['entry_ccode'];
  $i_entry_title = $_POST['entry_title'];
  $i_entry_summary = $_POST['entry_summary'];
  $i_entry_ftext = $_POST['entry_ftext'];
  $i_entry_cdate = $_POST['entry_cdate'];
//  $i_entry_author = $_POST['entry_author'];
  $i_entry_publish = $_POST['entry_publish'];
  if ($i_entry_publish == 'on')
  {
    $i_entry_publish = 1;
  }
  else
  {
    $i_entry_publish = 0;
  }
  $query='insert into '.$table.' (ccode, title, summary, ftext, cdate, publish) values ('.
    $i_entry_ccode.', "'.$i_entry_title.'", "'.$i_entry_summary.
	'", "'.$i_entry_ftext.'", "'.$i_entry_cdate.'", '.$i_entry_publish.');';
  $insertnewentry = mysql_query($query);
  $entryinsertno = mysql_affected_rows();
  $mymsg = '<br><br><b>'.$entryinsertno.'</b> رکورد ثبت شد.';

  redirect_rel('entries.php', $mymsg, 0);
}
//************************** INSERT NEW ENTRY - end

//************************** UPDATE ENTRY
function update_entry($e_entry_id)
{
	$table = 'entries';
	$query = 'select title from '.$table.' a where '.
		'a.id='.$e_entry_id;
	$e_entry = mysql_query($query);
	$num = mysql_num_rows($e_entry);
	if ($num == 0)
		$mymsg = 'رکوردی با این شماره پیدا نشد.';
	else
	{
		$e_entry_title = $_POST['entry_title'];
		$e_entry_ccode = $_POST['entry_ccode'];
		$e_entry_summary = $_POST['entry_summary'];
		$e_entry_ftext = $_POST['entry_ftext'];
		$e_entry_cdate = $_POST['entry_cdate'];
		//$e_entry_author = $_POST['entry_author'];
		$e_entry_publish = $_POST['entry_publish'];
		if ($e_entry_publish == 'on')
			$e_entry_publish = 1;
		else
			$e_entry_publish = 0;
		$query = 'update entries set '.
			'title="'.$e_entry_title.'", ftext="'.$e_entry_ftext.
			'", summary="'.$e_entry_summary.
			'", ccode='.$e_entry_ccode.', cdate="'.$e_entry_cdate.'", '.
			'publish='.$e_entry_publish.' where entries.id='.$e_entry_id;
		$entryedit = mysql_query($query);
		$no = mysql_affected_rows();
		$mymsg = '<br><br><b>تغییرات '.$no.'</b> رکورد ثبت شد.';
	}

	redirect_rel('entries.php', $mymsg, 0);
}
//************************** UPDATE ENTRY - end

//************************** VARDELM
function vardelm($pic_dir = '')
{
	$cc = $_REQUEST['cc'];
	if (($cc <> 1) and ($cc <> 2))
		$cc = $_POST['cc'];

	$table = $_POST['tablename'];
	if (!$table)
		$table = $_REQUEST['tablename'];
	$spage = $_POST['spage'];
	if (!$spage)
		$spage = $_REQUEST['spage'];
	$d_id = $_REQUEST['id'];
	$d_ids = $_POST['ids'];
	$m = 0;

	if ($d_id)
	{
		if ($pic_dir != '')
		{
			//@unlink($pic_dir.
				//$d_id.'-'.get_album_id($d_id).'.jpg');
			//delete_from_album($eid);
		}
		$m += delete_var($table, $d_id);
	}
	else
		if ($d_ids)
			foreach($d_ids as $index=>$on)
			{
				if ($pic_dir != '')
				{
					//@unlink($pic_dir.
						//$index.'-'.get_album_id($index).'.jpg');
					//delete_from_album($eid);
				}
				$m += delete_var($table, $index);
			}

	if (!$m)
		$mymsg = 'ركوردي با اين شماره پيدا نشد و يا قابل حذف نبود.<br>';
	else
		$mymsg = $m.' ركورد حذف شد.<br>';

	echo $mymsg;
}
//************************** VARDELM - end

//************************** GET RECORD
function get_record($table, $id)
{
	$q = 'select * from '.$table.' a where a.id='.$id;
	$q = mysql_query($q);
	if (mysql_num_rows($q))
	{
		$q = mysql_fetch_object($q);
		return $q;
	}
	else
	return 0;
}
//************************** GET RECORD - end

//************************** UPLOAD FILE
function upload_file($target_dir, $tmp = 0)
{
  $userfile=$_FILES['userfile']['tmp_name'];
  $userfile_name=$_FILES['userfile']['name'];
  $userfile_size=$_FILES['userfile']['size'];
  $userfile_type=$_FILES['userfile']['type'];
  $userfile_error=$_FILES['userfile']['error'];
  $filename = basename($userfile_name);
  if (!$tmp)
    $t = TMP_PIC;
  else
    $t = $filename;
  if ($userfile_size<=0)
    echo "$filename خالي است يا امكان بارگذاري آن وجود ندارد.<br>";
  if (!@copy($userfile, "$target_dir$t"))
  {
    echo "عدم توانايي در كپي فايل.<br>";
    return;
  }
 // echo "$filename با موفقيت بارگذاري شد.<br>";
//  echo "File size: ".number_format($userfile_size)."<br>";
  //echo "File type: $userfile_type<br>";
  return $target_dir.$filename;
}
//************************** UPLOAD FILE - end

//************************** INSERT NEW HADITH
function insert_new_hadith()
{
	$table = 'hadithha';
	$i_hadith_title = $_POST['hadith_title'];
	$i_hadith_speaker = $_POST['hadith_speaker'];
	$i_hadith_arabic = $_POST['hadith_arabic'];
	$i_hadith_farsi = $_POST['hadith_farsi'];
	$i_hadith_refrence = $_POST['hadith_refrence'];
	$query='insert into hadithha (title, speaker, arabic, farsi, refrence) values ('.
		'"'.$i_hadith_title.'", "'.$i_hadith_speaker.'", "'.
		$i_hadith_arabic.'", "'.$i_hadith_farsi.'", "'.$i_hadith_refrence.'");';
	$insertnewhadith = mysql_query($query);
	$hadithinsertno = mysql_affected_rows();
	$mymsg = '<br><br><b>'.$hadithinsertno.'</b> رکورد ثبت شد.<br>';

	redirect_rel('hadithha.php', $mymsg, 0);
}
//************************** INSERT NEW HADITH - end

//************************** GET NUM OF RECORDS
function get_num_of_records($table = 'entries', $publish = 1, $c = 0)
{
	if (!$c)
		$cf = ';';
	else
		$cf = ' and a.ccode='.$c.';';
	$q = 'select id from '.$table.' a where a.publish='.$publish.$cf;
	$q = mysql_query($q);
	return mysql_num_rows($q);
}
//************************** GET NUM OF RECORDS - end

//************************** GET USER NNAME
function get_user_nname($user_name)
{
  $q = 'select name from authors a where a.uname="'.$user_name.'"';
  $q = mysql_query($q);
  if (@mysql_num_rows($q))
  {
    $q = mysql_fetch_object($q);
    return $q->name;
  }
  else
    return false;
}
//************************** GET USER NNAME - end

//************************** LOGIN
function login($user_name, $user_pass)
{
  if (isset($_SESSION['v_user']))
    return false;
  mydbconnect();
  $q = 'select * from authors a where a.uname="'.$user_name.
    '" and a.pass=password("'.$user_pass.'");';
  $q = mysql_query($q);
  if (@mysql_num_rows($q))
    return true;
  return false;
}
//************************** LOGIN - end

//************************** IS FILLED
function is_filled($f)
{
  foreach ($f as $i=>$v)
    if (!isset($i) || ($v == ''))
      return false;
  return true;
}
//************************** IS FILLED - end

//************************** INSERT NEW AUTHOR
function insert_new_author()
{
  $table= 'authors';
  $i_author_name = $_POST['author_name'];
  $i_author_uname = $_POST['author_uname'];
  $i_author_pass = $_POST['author_pass'];
  $i_author_passc = $_POST['author_passc'];
  if ($i_author_pass != $i_author_passc)
    $mymsg = 'دو روز عبور يكسان نيستند.';
  else
  {
    if (!is_filled($_POST))
      $mymsg = 'مشكل در فيلدها: همه فيلدها بايد پر شوند.';
    else
    {
	  $q = 'select uname from authors a where uname="'.$i_author_uname.'"';
	  $q = mysql_query($q);
	  if (@mysql_num_rows($q))
	    $mymsg = '<br><br>اين نام كاربري تكراري است.';
	  else
	  {
        $query='insert into '.$table.' (name, uname, pass) values ('.
          '"'.$i_author_name.'", "'.$i_author_uname.
          '", password("'.$i_author_pass.'"));';
        $insertnewauthor = mysql_query($query);
        $authorinsertno = mysql_affected_rows();
        $mymsg = '<br><br><b>'.$authorinsertno.'</b> رکورد ثبت شد.';
	  }
    }
  }
  redirect_rel('authors.php', $mymsg, 0);
}
//************************** INSERT NEW AUTHOR - end

//************************** UPDATE AUTHOR
function update_author($e_author_id)
{
  $table = 'authors';
  $query = 'select uname from '.$table.' a where '.
    'a.id='.$e_author_id;
  $e_author = mysql_query($query);
  $num = mysql_num_rows($e_author);
  if ($num == 0)
    $mymsg = 'رکوردی با این شماره پیدا نشد.';
  else
  {
    $e_author_name = $_POST['author_name'];
    $e_author_uname = $_POST['author_uname'];
    $e_author_pass_now = $_POST['author_pass_now'];
    $e_author_pass_new = $_POST['author_pass_new'];
    $e_author_pass_new_c = $_POST['author_pass_new_c'];
    if ($e_author_pass_new != $e_author_pass_new_c)
      $mymsg = 'دو روز عبور يكسان نيستند.';
    else
    {
      if (!is_filled($_POST))
        $mymsg = 'مشكل در فيلدها: همه فيلدها بايد پر شوند.';
      else
      {
        $p = 'select pass from authors a where a.id='.$e_author_id.
          ' and pass=password("'.$e_author_pass_now.'")';
        $p = mysql_query($p);
        if (@mysql_num_rows($p))
        {
          $query = 'update authors set '.
            'name="'.$e_author_name.'", uname="'.$e_author_uname.
            '", pass=password("'.$e_author_pass_new.'") '.
            ' where authors.id='.$e_author_id;
          $authoredit = mysql_query($query);
          $no = mysql_affected_rows();
          $mymsg = '<br><br><b>تغییرات '.$no.'</b> رکورد ثبت شد.';
          $e_author = mysql_fetch_object($e_author);
          if ($_SESSION['v_user'] == $e_author->uname)
            $_SESSION['v_user'] = $e_author_uname;
        }
        else
          $mymsg = '<br><br>رمز عبور اشتباه است.';
      }
    }
  }

  redirect_rel('authors.php', $mymsg, 0);
}
//************************** UPDATE AUTHOR - end

//************************** DEL AUTHOR
function del_author($d_author_id)
{
  $m_pass = $_POST['m_pass'];
  if ($m_pass && is_this_manager($m_pass))
  {
    $d = 'delete from authors where authors.id='.$d_author_id;
    $d = mysql_query($d);
    if ($n = mysql_affected_rows())
      $mymsg = '<br><br><b>'.$n.'</b> ركورد حذف شد.';
    else
      $mymsg = '<br><br>خطا در حذف';
  }
  else
    $mymsg = 'شما اجازه اين كار را نداريد.';
  
  redirect_rel('authors.php', $mymsg, 0);
}
//************************** DEL AUTHOR - end

//************************** IS THIS MANAGER
function is_this_manager($pass = '')
{
  if ($pass != '')
    $passfilter = ' and a.pass=password("'.$pass.'") ';
  else
    $passfilter = '';
  $is_m = 'select * from authors a where a.uname="'.$_SESSION['v_user'].
    '" and a.manager=1'.$passfilter;  // is manager ?
  $is_m = mysql_query($is_m);
  if (@mysql_num_rows($is_m))
    return true;
  else
    return false;
}
//************************** IS THIS MANAGER - end

?>