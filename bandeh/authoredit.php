﻿<? @session_start(); ?>
<html dir="rtl">
<?php
require_once('inc.php');
if (!isset($_SESSION['v_user']))
  redirect_rel('index.php', '', 0);
else
{
html_top('نويسندگان', 'ويرايش اطلاعات نويسنده');

$e_author_id = $_REQUEST['id'];
$editauthor_submit = $_POST['editauthor_submit'];
if ($editauthor_submit)
  update_author($e_author_id);
else
  edit_author_form($e_author_id);

html_bottom();
}
?>