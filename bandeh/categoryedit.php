﻿<? @session_start();
require_once('inc.php');
if (!isset($_SESSION['v_user']))
	redirect_rel('index.php', '', 0);
else
{
html_top('', 'ويرايش دسته');

$e_category_id = $_REQUEST['id'];
$editcategory_submit = $_POST['editcategory_submit'];
if ($editcategory_submit)
	update_category($e_category_id);
else
	edit_category_form($e_category_id);

html_bottom();
}
?>