<?php
/////////////////////////////////////// MAIN VARS - most of them are for database
$main_url = "http://localhost/bandeh/";
$hostname_bandeh = "localhost";
$database_bandeh = "bandeh";
$username_bandeh = "root";
$password_bandeh = "";
///////////////////////////////////////

define('ARCHIVE_PAGE', 'http://localhost/bandeh/archive.php');
define('NSUMCHAR', 50);
define('NSUMCHAR_BIG', 500);
define('TMP_PIC_DIR', 'images/tmp/');
define('TMP_PIC', 'tmp.jpg');
define('MARTYRS_PIC_DIR', 'images/b/martyrs/');
define('NEWS_PIC_DIR', 'images/b/news/');
define('ASAR_PIC_DIR', 'images/b/asar/');
define('FILES_DIR', 'images/b/files/');
define('REMEMBRANCES_PIC_DIR', 'images/b/remembrances/');
define('NOTES_IN_PAGE', 15);
define('FIRST_NO', 5); // Number of records in the first page of each topic
?>